<?php
/*
Description: Provides template functions, and shortcodes for protecting mailto: and tel: links both on client and server side
Author: Loïc Pennamen
*/

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

// The class handles all the stuff
include_once 'ProtectedLink.class.php';

// Init plugin
function protectedlinks_init(){

	// Add JS
	$jsDir = get_template_directory_uri() . '/vendor/loicpennamen/wpprotectedlinks/js/';
	wp_register_script('protectedlinks.js', $jsDir . 'protectedlinks.js');
	wp_enqueue_script('protectedlinks.js');
	
	// Add shortcode
	add_shortcode( 'protected-link', 'wpprotectedlinks_shortcode' );

}

// Shorthand to get encoded e-mail as string
function protectedlink($emailOrPhoneNumber, $type = 'email', $acfDomain = null){
	
	// Search for acf value is exists
	if(function_exists('get_field')){
		$acfValue = get_field($emailOrPhoneNumber, $acfDomain);
		if($acfValue)
			$emailOrPhoneNumber = $acfValue;
	}
	
	// Init the object
	$protectedLink = new ProtectedLink();

	// Return the HTML/JS script
	return $protectedLink->getLink($emailOrPhoneNumber, $type);
}

// Shortcode version
function wpprotectedlinks_shortcode($atts){
	
	$a = shortcode_atts( array(
		'value' => null,
		'acf-domain' => null,
		'type' => null,
	), $atts );
	
	if(!$a['value'])
		return "<!-- wpprotectedlink: 'value' is missing -->";
	
	return protectedlink($a['value'], $a['type'], $a['acf-domain']);
}

// Init plugin
add_action('init', 'protectedlinks_init');
