<?php

class ProtectedLink{

	// ////
	// PUBLIC METHODS

	public function __construct(){
	}

	// Get HTML script tag
	public function getLink($target, $type = null){

		// JS script:
		$json = $this->toJson($target, $type);
		
		$js = 'LP.protectedLinks.getLink(' . $json . ' '.($type ? ', \''.$type.'\'' : '').')';
		
		return $this->wrapJs($js);
	}

	// Wrap string in javascript HTML tag
	private function wrapJs($str){
		return '<script type="text/javascript">' . $str . '</script>';
	}

	// Get JSON data from email or phone string
	private function toJson($value, $type){
		switch($type){
			case 'tel':
				return json_encode( [$value] );
				break;
			default: // email
				return json_encode( $this->splitString($value) );
		}
	}

	// Split string to array of unclear data unusable by mean spamming bots
	private function splitString($email){
		$tmp = explode('@', $email);
		$data[] = $tmp[0];
		$data[] = substr($tmp[1], 0, strrpos($tmp[1], '.'));
		$data[] = substr($tmp[1], strrpos($tmp[1], '.'));
		
		// ASCII encoding of characters
		for ($i = 0; $i < sizeof($data); $i++)
			$data[$i] = $this->encodeStr($data[$i]);

		return $data;
	}

	// Encode a string in ASCII
	private function encodeStr($str){
		$output = '';
		for ($i = 0; $i < strlen($str); $i++)
			$output .= '&#'.ord($str[$i]).';';
		return $output;
	}
}