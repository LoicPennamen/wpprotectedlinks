## Wordpress protected links: mailto | tel

Protects e-mails and phone numbers from spam in a HTML document by both server and client-side encryption. 
Designed for personal use in custom wordpress themes, with composer.